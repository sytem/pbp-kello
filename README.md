# PBP-kello-bridge

Small proxy-program to read playback status from PlaybackPro ( https://www.dtvideolabs.com/dt-videolabs-playbackpro-professional-video-playback/) and send it to https://gitlab.com/clock-8001/clock-8001 while control it via https://bitfocus.io/companion

PBP offers TCP and UDP remote, first is one client only, second gives no information what return values are. So need to combine both and proxy commands from UDP while poll status and send it via OSC. API-documentation of PBP: https://www.dtvideolabs.com/resources/PlaybackProPlusUserGuide.pdf

Use Mitti format ( https://imimot.com/help/mitti/external-controls/ )to send data to Clock-8001

Mac-build can be found: http://sytem.fi/pbp-kello/

# Usage

If running on same mac as PBP and clock is listening broadacast, some timer set to listen port 21245, usage is:

Install:

- download file from above link
- open terminal
- mv Downloads/pbp-kello-mac-v<version> .
- chmod a+x pbp-kello-mac-v<version>

Usage:
- open terminal if needed
- ./pbp-kello-<version>


If needed, add parameters:

  -clock string
        IP-Address of Clock-8001 to send OSC, default broadcast (default "255.255.255.255")
  -clockport int
        port to send OSC, check what timer<N> is listening at clock-8001 (default 21245)
  -pbp string
        Address (and port if needed) of PBP, default port TCP 4647 (default "127.0.0.1")


# version history

## v02 2022

- Basic functions to use in productions

## v03 09/2023

- Change from legacy api to Mitti protocol
- Fixed still images crashing proxy
- Add timeout on zero to allow multiple timers on same source display

## v04 09/2024
- adjust pbp-timeouts to get proxy better
- try to fix crash on replys arriving wrong order

## v05 09/2024
- smaller delay so clock wont timeout us between messages

# Contact

Teppo Rekola, sytem @ iki.fi
