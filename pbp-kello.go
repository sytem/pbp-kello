/*
sytem@iki.fi 2022-2023

Proxy to send remaining time from PlaybackPro to Clock-8001 and remote commands from Bitfocus Companion to PBP

2023 version: send data as mitti as old api has been removed

Open connection to PBP with TCP, listen UDP, and send OSC out

General TODO:
	unified logging
	unified error handling
*/

package main

import (
	"flag"
	"fmt"
	"net"
	"os"
	"strconv"
	"strings"
	"time"

	"gitlab.com/Depili/go-osc/osc"
)

func main() {

	PBPserverPtr := flag.String("pbp", "127.0.0.1", "Address (and port if needed) of PBP, default port TCP 4647")
	ClockAddrPtr := flag.String("clock", "255.255.255.255", "IP-Address of Clock-8001 to send OSC, default broadcast")
	ClockPortPtr := flag.Int("clockport", 21245, "port to send OSC, check what timer<N> is listening at clock-8001")

	flag.Parse()

	//default PBP port if no port given
	if !strings.Contains(*PBPserverPtr, ":") {
		*PBPserverPtr = fmt.Sprintf("%v:4647", *PBPserverPtr)
	}

	tcpAddr, err := net.ResolveTCPAddr("tcp", *PBPserverPtr)
	if err != nil {
		println("ResolveTCPAddr failed:", err.Error())
		os.Exit(1)
	}

	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	if err != nil {
		//println("Dial failed:", err.Error())
		println("Connection to PBP failed, is it listening TCP-remote? :", err.Error())
		os.Exit(1)
	}

	defer conn.Close()

	oscClient := osc.NewClient(*ClockAddrPtr, *ClockPortPtr)
	println("send clock-8001 OSC-messages to:", *ClockAddrPtr, ":", *ClockPortPtr)

	//start listening for companion messages
	go proxy(conn)

	// to not send repeated messages for playstatus
	lastPaused := true
	// to not send zeros all the time
	zeroSent := false

	//main loop to poll and send state constantly
	for {

		//send commands to poll data
		PSreply := queryServer("PS", conn) // playback status
		TRreply := queryServer("TR", conn) // elapsed time
		GDreply := queryServer("GD", conn) // file time

		println("Reply from PBP:", PSreply, " : ", TRreply, "/", GDreply)

		//init zero if not playing
		TRhours := 0
		TRminutes := 0
		TRseconds := 0
		TRframes := 0
		GDhours := 0
		GDminutes := 0
		GDseconds := 0
		GDframes := 0

		paused := true
		//looping := false // TODO check for this

		allZero := true

		// keep zero if times are "N/A", == not playing
		if !strings.Contains(PSreply, "N/A") && !strings.Contains(GDreply, "N/A") && !strings.Contains(TRreply, "N/A") {


			//if not valid format, zero all and return (must be wrong data, incorrect order or something. can wait for next)
			if !strings.Contains(GDreply, ":") || !strings.Contains(TRreply, ":") {
				println("No : in time, drop it")
				time.Sleep(500 * time.Millisecond)
				continue
			}	

			//split TR and GD value to parts
			TRhours, TRminutes, TRseconds, TRframes = splitTime(TRreply)
			GDhours, GDminutes, GDseconds, GDframes = splitTime(GDreply)

			// TODO do we need to handle faster/slower/backwards playback somehow here?

			//if we are here, we are playing
			paused = false

			//and have something to update
			allZero = false

			//unless we are not
			if strings.Contains(PSreply, "0.00") {
				paused = true
			}

		}

		// dont repeat zero so clock-8001 can timeout source and display something else
		if allZero && zeroSent {
			time.Sleep(500 * time.Millisecond)
			continue
		}

		//send OSC as mitti (https://imimot.com/help/mitti/external-controls/)

		oscMsg1 := osc.NewMessage("/mitti/cueTimeElapsed")

		// lets do math, count how much have been elapsed when we know file time and remaining

		Ehours := GDhours - TRhours
		Eminutes := GDminutes - TRminutes
		Eseconds := GDseconds - TRseconds
		Eframes := GDframes - TRframes

		//needs zero padding
		Etime := padNumberWithZero(Ehours) + ":" + padNumberWithZero(Eminutes) + ":" + padNumberWithZero(Eseconds) + ":" + padNumberWithZero(Eframes)

		oscMsg1.Append(Etime)
		oscClient.Send(oscMsg1)

		oscMsg2 := osc.NewMessage("/mitti/cueTimeLeft")

		//build string from pieces because there might be garbage at the end of the reply from pbp, and need "-" at start

		Ltime := "-" + padNumberWithZero(TRhours) + ":" + padNumberWithZero(TRminutes) + ":" + padNumberWithZero(TRseconds) + ":" + padNumberWithZero(TRframes)

		oscMsg2.Append(Ltime)
		oscClient.Send(oscMsg2)

		if paused != lastPaused {

			oscMsg3 := osc.NewMessage("/mitti/togglePlay")
			if paused {
				oscMsg3.Append(int32(0))
			} else {
				oscMsg3.Append(int32(1))
			}

			oscClient.Send(oscMsg3)
			lastPaused = paused

		}

		//reset conter, if we are here for first zero --> 1, other data --> 0
		zeroSent = allZero

		time.Sleep(500* time.Millisecond) // TODO is this good polling interwall?
	}

}

func proxy(conn net.Conn) {
	//udp server to listen default port of pbp :7000
	// if this run on pbp-mac, and no proxy is running on some moment, Companion can send directly to pbp with just changing PBP remote-listening from tcp --> udp
	serverPort := 7000
	addr := net.UDPAddr{
		Port: serverPort,
		IP:   net.ParseIP("0.0.0.0"),
	}

	server, err := net.ListenUDP("udp", &addr)
	if err != nil {
		fmt.Printf("UDP-proxy listen err %v\n", err)
		os.Exit(-1)
	}

	fmt.Printf("UDP-proxy listening at %v\n", addr.String())
	defer server.Close()

	// proxy loop
	for {
		p := make([]byte, 1024)
		nn, raddr, err := server.ReadFromUDP(p)
		if err != nil {
			fmt.Printf("UDP Read err  %v", err)
			continue
		}

		msg := p[:nn]

		fmt.Printf("Received from companion %v %s\n", raddr, msg)

		//resend tcp
		 commandServer(string(msg), conn) 

	}
}

func splitTime(replyMsg string) (hours, minutes, seconds, frames int) {

	//fmt.Println(replyMsg)

	hours, err := strconv.Atoi(strings.Split(replyMsg, ":")[0])
	if err != nil {
		println("bad hours")
		fmt.Println(err)
		hours = 0
	}

	minutes, err = strconv.Atoi(strings.Split(replyMsg, ":")[1])
	if err != nil {
		println("bad minutes")
		fmt.Println(err)
		minutes = 0
	}

	seconds, err = strconv.Atoi(strings.Split(replyMsg, ":")[2])
	if err != nil {
		println("bad seconds")
		fmt.Println(err)
		seconds = 0
	}

	frames, err = strconv.Atoi(strings.Split(replyMsg, ":")[3][:2]) //use only 2 first numbers, some garbage at end of reply
	if err != nil {
		println("bad frames")
		fmt.Println(err)
		frames = 0
	}
	return
}

func queryServer(msg string, conn net.Conn) (reply string) {
	// Set timeout for the server response
	conn.SetDeadline(time.Now().Add(50 * time.Millisecond)) // TODO is this good value, lot smaller than main loop delay

	_, err := conn.Write([]byte(msg))
	if err != nil {
		println("Write to PBP failed:", err.Error())
		os.Exit(1)
	}

	replyBytes := make([]byte, 1024)

	_, err = conn.Read(replyBytes)
	if err != nil {
		if netErr, ok := err.(net.Error); ok && netErr.Timeout() {
			// Handle timeout error, return "N/A" so upper logic can skip
			fmt.Println("PBP response timed out")
			reply = "N/A"
			return
		}
		println("read from PBP failed:", err.Error())
		os.Exit(1)
	}

	reply = string(replyBytes)
	//println("reply from server=", reply)
	return
}

func commandServer(msg string, conn net.Conn) {

	// Set timeout for the server response
	conn.SetDeadline(time.Now().Add(10 * time.Millisecond)) // TODO is this good value, lot smaller than main loop delay

	_, err := conn.Write([]byte(msg))
	if err != nil {
		println("Write to PBP failed:", err.Error())
		os.Exit(1)
	}

}

func padNumberWithZero(value int) string {
	return fmt.Sprintf("%02d", value)
}
