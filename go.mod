module gitlab.com/sytem/pbp-kello

go 1.17

require gitlab.com/Depili/go-osc v0.0.0-20220126123138-cdbd92c9027a

require (
	github.com/denisbrodbeck/machineid v1.0.1 // indirect
	golang.org/x/sys v0.0.0-20220209214540-3681064d5158 // indirect
)
